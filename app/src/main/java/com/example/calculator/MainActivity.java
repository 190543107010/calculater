package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,bdiv,bmul,bmin,badd,bclr,beq;
    TextView textViewresult = null;
    TextView st;
    double val1, val2;

    boolean add, sub, div, mul;
    boolean equalcheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initviewReferanse();
        initEvent();
    }
    void initviewReferanse()
    {
        b1 = findViewById(R.id.button1);
        b2 = findViewById(R.id.button2);
        b3 = findViewById(R.id.button3);
        b4 = findViewById(R.id.button4);
        b5 = findViewById(R.id.button5);
        b6 = findViewById(R.id.button6);
        b7 = findViewById(R.id.button7);
        b8 = findViewById(R.id.button8);
        b9 = findViewById(R.id.button9);
        b0 = findViewById(R.id.button0);
        badd = findViewById(R.id.buttonadd);
        bmin = findViewById(R.id.buttonmin);
        bdiv = findViewById(R.id.buttondiv);
        bmul = findViewById(R.id.buttonmul);
        bclr = findViewById(R.id.buttonclar);
        beq = findViewById(R.id.buttoneq);
        textViewresult = findViewById(R.id.textViewresult);
        st = findViewById(R.id.stor);
    }
    void initEvent()
    {
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true) {
                    textViewresult.setText("");
                    equalcheck = false;
                } else {
                    textViewresult.setText(textViewresult.getText() + "1");
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true) {
                    textViewresult.setText("");
                    equalcheck = false;
                } else

                    textViewresult.setText(textViewresult.getText() + "2");
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true) {
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText() + "3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"9");
            }
        });
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equalcheck == true){
                    textViewresult.setText("");
                    equalcheck = false;
                }
                textViewresult.setText(textViewresult.getText()+"0");
            }
        });
        badd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewresult.getText() == "") {

                    textViewresult.setText("");
                } else {
                    val1 = Double.parseDouble(textViewresult.getText() + "+");
                    st.setText(null);
                    add = true;

                    textViewresult.setText(null);
                }
            }
        });
        bmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewresult.getText() == "") {

                    textViewresult.setText("");
                } else {
                    val1 = Double.parseDouble(textViewresult.getText() + "-");
                    st.setText(null);
                    sub = true;

                    textViewresult.setText(null);
                }
            }
        });
        bdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewresult.getText() == "") {

                    textViewresult.setText("");
                } else {
                    val1 = Double.parseDouble(textViewresult.getText() + "/");
                    st.setText(null);
                    div = true;

                    textViewresult.setText(null);
                }
            }
        });
        bmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewresult.getText() == "") {

                    textViewresult.setText("");
                } else {
                    val1 = Double.parseDouble(textViewresult.getText() + "*");
                    st.setText(null);
                    mul = true;

                    textViewresult.setText(null);
                }
            }
        });
        bclr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewresult.setText("");
            }
        });
        beq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewresult.getText() == "") {

                    textViewresult.setText("");
                } else {

                    equalcheck = true;
                    val2 = Double.parseDouble(textViewresult.getText() + "");
                    if (add == true) {

                        st.setText((int)val1);
                        textViewresult.setText((int) (val1+val2));
                        add = false;

                    }
                    if (sub == true) {
                        st.setText((int)val1);
                        textViewresult.setText((int) (val1 - val2));
                        sub = false;
                    }
                    if (mul == true) {
                        st.setText((int)val1);
                        textViewresult.setText(val1 * val2 + "");
                        mul = false;
                    }
                    if (div == true) {
                        textViewresult.setText(val1 / val2 + "");
                        div = false;
                    }

                }
            }
        });

    }
}
